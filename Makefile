%.sh: %.org
	emacs -Q --batch --eval "(progn (require 'org) (require 'ob-tangle) (org-babel-tangle-file \"$<\"))"

all: ziffshrc.sh

clean:
	rm -f *.sh

.PHONY: all clean
